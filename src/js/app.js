
var index_plato=0;
var arme_plato= [
	{"nombre": "Espinaca", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Lechuga", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Tomate", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Zanahoria", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Alcachofa", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Pepino", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Cebolla", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Pimentón", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Champiñones", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Rúcula", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Salmón", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Pechuga de pollo Asada/horno", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Cerdo a la plancha/horno", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Carne de res a la plancha", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Mojarra", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Tilapia", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Coliflor", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Espárragos", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Plátano asado", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Papa al horno", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Yuca asada/hervida", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Arroz", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Avena", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Repollo", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Cereal de caja", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Galletas", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Plátano frito", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Yuca frita", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Pollo frito", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Pastel de chocolate", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Carne frita", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Pasta", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Quinoa", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Frijol", "tipo": "Legumbres", "activado":0 },
	{"nombre": "Lenteja", "tipo": "Legumbres", "activado":0 },
	{"nombre": "Garbanzo", "tipo": "Legumbres", "activado":0 },
	{"nombre": "Empanada", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Aguacate", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Aceite de oliva", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Pan integral", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Hojaldra", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Queso bajo en grasa", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Aceitunas", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Maní", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Macadamias", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Almendras", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Mantequilla", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Margarina", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Coco", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Chicharrón", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Queso crema", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Suero", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Helado", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Yogurt azucarado", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Papas fritas", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Atún", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Huevo", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Queso doble crema", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Pizza", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Agua", "tipo": "Bebidas", "activado":0 },
	{"nombre": "Cerveza", "tipo": "Bebidas", "activado":0 },
	{"nombre": "Jugo con azúcar", "tipo": "Bebidas", "activado":0 },
	{"nombre": "Energizante", "tipo": "Bebidas", "activado":0 },
	{"nombre": "Soya", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Brócoli", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Calabacín", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Yogurt natural", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Leche", "tipo": "Lácteos", "activado":0 },
	{"nombre": "Salchichas", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Gaseosa", "tipo": "Bebidas", "activado":0 },
	{"nombre": "Mortadela", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Manzana", "tipo": "Frutas", "activado":0 },
	{"nombre": "Papaya", "tipo": "Frutas", "activado":0 },
	{"nombre": "Arándanos", "tipo": "Frutas", "activado":0 },
	{"nombre": "Fresas", "tipo": "Frutas", "activado":0 },
	{"nombre": "Ciruelas", "tipo": "Frutas", "activado":0 },
	{"nombre": "Moras", "tipo": "Frutas", "activado":0 },
	{"nombre": "Mango", "tipo": "Frutas", "activado":0 },
	{"nombre": "Banano", "tipo": "Frutas", "activado":0 },
	{"nombre": "Granadilla", "tipo": "Frutas", "activado":0 },
	{"nombre": "Piña", "tipo": "Frutas", "activado":0 },
	{"nombre": "Durazno", "tipo": "Frutas", "activado":0 },
	{"nombre": "Sandia", "tipo": "Frutas", "activado":0 },
	{"nombre": "Jugo de caja", "tipo": "Bebidas", "activado":0 },
	{"nombre": "Pan dulce", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Mazorca", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Salchichón", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Mantequilla", "tipo": "Grasas Saludables", "activado":0 },
	{"nombre": "Torta tres leches", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Frambuesas", "tipo": "Frutas", "activado":0 },
	{"nombre": "Naranja", "tipo": "Frutas", "activado":0 },
	{"nombre": "Mandarina", "tipo": "Frutas", "activado":0 },
	{"nombre": "Mango", "tipo": "Frutas", "activado":0 },
	{"nombre": "Kiwi", "tipo": "Frutas", "activado":0 },
	{"nombre": "Pera", "tipo": "Frutas", "activado":0 },
	{"nombre": "Acelga", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Ajo", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Berenjena", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Calabaza", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Auyama", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Apio", "tipo": "Verduras y Hortalizas", "activado":0 },
	{"nombre": "Sardinas enlatadas", "tipo": "Proteínas", "activado":0 },
	{"nombre": "Arepa de maíz", "tipo": "Carbohidratos", "activado":0 },
	{"nombre": "Papas a la francesa", "tipo": "Carbohidratos", "activado":0}
];

var rules_game = [
	{
		"tipo": "Verduras y Hortalizas",
		"value": 5,
	},
	{
		"tipo": "Carbohidratos",
		"value": 2,
	},
	{
		"tipo": "Proteínas",
		"value": 1,
	},
	{
		"tipo": "Bebidas",
		"value": 1,
	},
	{
		"tipo": "Grasas Saludables",
		"value": 1,
	}
];
var user_game =[
	{
		"tipo": "Verduras y Hortalizas",
		"value": 0,
	},
	{
		"tipo": "Carbohidratos",
		"value": 0,
	},
	{
		"tipo": "Proteínas",
		"value": 0,
	},
	{
		"tipo": "Bebidas",
		"value": 0,
	},
	{
		"tipo": "Grasas Saludables",
		"value": 0,
	}
];
var mov_permitidos = 9;

window.onload = function() {
    // Código a ejecutar cuando se ha cargado toda la página
  document.body.style.visibility = "visible";
   // stage2.stop();
    main();
};
var audios = [
	{
    url: "./sounds/click.mp3",
    name: "clic"
	},{
	url: "./sounds/good.mp3",
	name: "good"
	},{
	url: "./sounds/bad.mp3",
	name: "bad"	
	}
];
ivo.info({
    title: "Arme su propio plato",
    autor: "Edilson Laverde Molina",
    date: "15/05/2023",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var stage1 = new TimelineMax();
var stage2 = new TimelineMax();
var stage3 = new TimelineMax();
function main() {
    var t = null;
    var udec = ivo.structure({
        created: function () {

            t = this;
            t.iniArmePlato();
            t.animations();
            t.events();
            //precarga audios//
            var onComplete = function () {
               
                stage1.play();
				document.body.classList.add("active");
            };
            ivo.load_audio(audios,onComplete );
        },
        methods: {
			showNext: function(){
				index_plato += 8;
				if(index_plato >= arme_plato.length){
					index_plato -= 8;
				}
				t.setArmePlato();
				
			},
			showBack: function(){
				index_plato -= 8;
				if(index_plato < 0){
					index_plato = 0;
				}else{
					t.setArmePlato();
				}
			},
			setArmePlato: function(){
				//agregamos los primeros 8 elemntos dle plato
				var plato = [];
				plato = arme_plato.slice(index_plato, index_plato+8);
				ivo(".items").hide();
				for (var i = 0; i < plato.length; i++) {
					if(plato[i].activado == 0){
						ivo("#"+plato[i].id).show();
					}
				}

			},
			iniArmePlato: function(){
				//agregamos todos los elementos dejamos visibles los 8 primeros
				ivo("#stage3_options").html("");
				var html = "";
				var item=1;
				for(var i = 0; i < arme_plato.length; i++){
					let id="item"+(i+1);
					arme_plato[i].id = id;
					html += `<div id="${id}" class="items item${item}" data-tipo="${arme_plato[i].tipo}">${arme_plato[i].nombre}</div>`;
					item++;
					if(item > 8){
						item = 1;
					}
				}
				ivo("#stage3_options").html(html);
				this.setArmePlato();
				let _this = this;
				ivo("#stage3_btn_up").on("click", function(){
					_this.showBack();
				});
				ivo("#stage3_btn_down").on("click", function(){
					_this.showNext();
				});	
				$(".items").draggable({
					revert: true,
					drag: function(event, ui) {
						// Centrar el cursor en el centro del elemento mientras se arrastra
						$(this).draggable("option", "cursorAt", {
						  top: Math.floor($(this).height() / 2),
						  left: Math.floor($(this).width() / 2)
						});
					}
				});
				$(".plato").droppable({
					accept: ".items",
					drop: function(event, ui) {
						mov_permitidos--;
						
						let title = $(this).attr("id").replace("dropp_", "");
						$("#"+title).removeClass("active_item");
						//comparamos el tipo del origen con el destino
						let tipo_origen = ui.draggable.attr("data-tipo");
						let tipo_destino = $(this).attr("data-tipo");
						//ocultamol el arrastrable
						ui.draggable.hide();
						//buscamos el elemento en el arreglo
						for(var i = 0; i < arme_plato.length; i++){
							if(arme_plato[i].id == ui.draggable.attr("id")){
								arme_plato[i].activado = 1;
							}
						}
						//buscamos user_game le suma 1 a la opcion elegida
						for(var i = 0; i < user_game.length; i++){
							if(user_game[i].tipo == tipo_origen){
								user_game[i].value += 1;
							}
						}

						if(tipo_origen == tipo_destino){
							//si son iguales
							ivo.play("good");
						}else{
							//si son diferentes
							ivo.play("bad");
						}
						if(mov_permitidos < 0) {
							//fin juego no debemos permitir mas dragg
							$(".items").draggable("disable");
							//conparamos user_game con rules_game
							let win = true;
							for(var i = 0; i < user_game.length; i++){
								if(user_game[i].value != rules_game[i].value){
									console.log(user_game[i].value+" "+rules_game[i].value);
									win = false;
								}
							}
							setTimeout(function(){
								if(win){
									ivo.play("good");
								}else{
									ivo.play("bad");
								}
							}, 3000);
						}
					},
					over: function(event, ui) {
						let title = $(this).attr("id").replace("dropp_", "");
						$("#"+title).addClass("active_item");
						console.log(title); 
					},
					out: function(event, ui) {	
						let title = $(this).attr("id").replace("dropp_", "");
						$("#"+title).removeClass("active_item");
					}
				});
			},
            events: function () {
               
                ivo("#stage1_btn_next").on("click", function () {
                    stage1.timeScale(3).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage2_btn_next").on("click", function () {
                    stage2.timeScale(3).reverse();
                    stage3.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage2_btn_back").on("click", function () {
                    stage2.timeScale(3).reverse();
                    stage1.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage3_btn_back").on("click", function () {
                    stage3.timeScale(3).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                });
                
            },
            animations: function () {

                stage1.append(TweenMax.from("#stage1", .8, {y: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from("#stage1_icon_splah", .8, {x: -300, opacity: 0}), 0);
                stage1.append(TweenMax.from("#stage1_btn_next", .8, {x: -300,rotation:300, opacity: 0,rotation:900}), 0);
                stage1.stop();
              
                stage2.append(TweenMax.from("#stage2", .8, {y: 7300, opacity: 0}), 0);
                stage2.append(TweenMax.from("#stage2_title", .8, {x: 700, opacity: 0}), 0);
                stage2.append(TweenMax.from("#stage2_text", .8, {x: 700, opacity: 0}), 0);
                stage2.append(TweenMax.from("#stage2_btn_back", .4, {x: 700, opacity: 0}), 0);
                stage2.append(TweenMax.from("#stage2_btn_next", .4, {x: 700, opacity: 0}), -.3);
                stage2.append(TweenMax.from("#stage2_sub_titulo", .4, {y: 700, opacity: 0}), -.1);
                stage2.stop();
              
                stage3.append(TweenMax.from("#stage3", .8, {y: 7300, opacity: 0}), 0);
                stage3.append(TweenMax.staggerFrom(".label", .8, {scale: 0,rotation:900, opacity: 0}, .2), 0);

				stage3.append(TweenMax.staggerFrom(["#stage3_btn_up","#stage3_options","#stage3_btn_down","#stage3_btn_back"], .8, {x:700, opacity: 0}, .2), 0);

                stage3.stop();
                

            }
        }
    });
}