import './src/scss/main.scss'
import './src/js/howler.min.js'
import './src/js/gsap.js'
import './src/js/jquery-3.7.0.min.js'
import './src/js/jquery-ui.min.js'
import './src/js/ivo.js'
import './src/js/app.js'

